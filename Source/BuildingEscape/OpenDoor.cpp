// CopyRight Ricky

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Components/PrimitiveComponent.h"

#define OUT
// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();	
	Owner = GetOwner();
}

void UOpenDoor::OpenDoor() {
	
	FRotator NewRotator = FRotator(0.f, -OpenAngle, 0.0f);
	Owner->SetActorRotation(NewRotator);
}

void UOpenDoor::CloseDoor() {
	FRotator NewRotator = FRotator(0.f, 0, 0.0f);
	Owner->SetActorRotation(NewRotator);
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// poll the trigger volume..
	if (PressurePlate && GetTotalMassOfActorOnPlate() >= TriggerMass) {
		// if the actor that opens is in the volume.
		OpenDoor();
		DoorLastOpenTime = GetWorld()->GetTimeSeconds();
	}else if ((GetWorld()->GetTimeSeconds() - DoorLastOpenTime) >= DoorCloseDelay) {
		CloseDoor();
	}
}

float UOpenDoor::GetTotalMassOfActorOnPlate() const{
	float TotlaMass = 0.f;
	TArray<AActor*> OverlappingActors;
	// Find all the overlaping actors.
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);
	// Iterate through them adding their mass.
	for (AActor* Actor : OverlappingActors) {
		UPrimitiveComponent* Component = Actor->FindComponentByClass<UPrimitiveComponent>();
		TotlaMass += Component->GetMass();
	}
	return TotlaMass;
}

