// CopyRight Ricky

#include "Grabber.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = GetWorld()->GetFirstPlayerController();
	UE_LOG(LogTemp, Warning, TEXT("Grabber reporting for duty"));
	
	FindPhysicsComponent();
	SetupInputComponent();
}

void UGrabber::FindPhysicsComponent() {
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (!PhysicsHandle) {
		UE_LOG(LogTemp, Error, TEXT("[%s]PhysicsHandle is missing!!!"), *(GetOwner()->GetName()));
	}
}

void UGrabber::SetupInputComponent() {
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent) {
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("[%s]InputComponent is missing!!!"), *(GetOwner()->GetName()));
	}
}

void UGrabber::Grab() {
	FHitResult Hit = GetFirstPhysicsBodyInReach();
	if (!Hit.GetComponent() || PhysicsHandle->GrabbedComponent) {
		return;
	}
	
	auto ComponentToGrab = Hit.GetComponent();
	PhysicsHandle->GrabComponentAtLocationWithRotation(
		ComponentToGrab,
		NAME_None,
		ComponentToGrab->GetOwner()->GetActorLocation(),
		ComponentToGrab->GetOwner()->GetActorRotation()
	);

	UE_LOG(LogTemp, Warning, TEXT("[%s] try to grab %s."), *(GetOwner()->GetName()), *(PhysicsHandle->GrabbedComponent->GetOwner()->GetName()));
}

void UGrabber::Release() {
	PhysicsHandle->ReleaseComponent();
	UE_LOG(LogTemp, Warning, TEXT("[%s] try to release something."), *(GetOwner()->GetName()));
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const {

	FVector ReachLineStart = GetReachLineStart();

	// Set up query parameters
	FCollisionQueryParams TraceParams = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	// Line-trace 
	FHitResult Hit;
	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		ReachLineStart,
		GetReachLineEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	);

	return Hit;
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (PhysicsHandle->GrabbedComponent) {
		
		PhysicsHandle->SetTargetLocation(GetReachLineEnd());
	}

}


FVector UGrabber::GetReachLineStart() const {
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	PlayerController->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation;
}

FVector UGrabber::GetReachLineEnd() const {
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	PlayerController->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation + (Reach * (PlayerViewPointRotation.Vector()));
}