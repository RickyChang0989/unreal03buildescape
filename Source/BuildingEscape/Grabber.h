// CopyRight Ricky

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "Components/PrimitiveComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GameFramework/PlayerController.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void SetupInputComponent();

	void FindPhysicsComponent();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(VisibleAnyWhere)
	APlayerController* PlayerController;

	UPROPERTY(EditAnyWhere)
	float Reach = 100.f;

	UPROPERTY(VisibleAnyWhere)
	UPhysicsHandleComponent* PhysicsHandle = nullptr;

	UPROPERTY(VisibleAnyWhere)
	UInputComponent* InputComponent = nullptr;

	// Ray-cast and grab whats in reach.
	void Grab();
	void Release();

	// Return hit for first physics body
	FHitResult GetFirstPhysicsBodyInReach() const;
	FVector GetReachLineStart() const;
	FVector GetReachLineEnd() const;
};
